from smbus import SMBus
import sys
from time import sleep
from math import atan,pi

global gyro_addr
gyro_addr = 0x68 # i2c addres of the gyro

global g

def neg(val):
    val = (~(val | 0xffff0000)) & 0x80007fff
    return -val

def gyro__init():
    global g
    g = SMBus(0) # /dev/i2c-0
    g.write_byte_data(gyro_addr,62,0x02) # set PLL with X Gyro reference
    g.write_byte_data(gyro_addr,22,0x18)
def gyro_read():
    global g
    X_MSB = g.read_byte_data(gyro_addr,29)
    X_LSB = g.read_byte_data(gyro_addr,30)
    
    Y_MSB = g.read_byte_data(gyro_addr,31)
    Y_LSB = g.read_byte_data(gyro_addr,32)

    Z_MSB = g.read_byte_data(gyro_addr,33)
    Z_LSB = g.read_byte_data(gyro_addr,34)

    x = X_LSB | (X_MSB << 8)
    y = Y_LSB | (Y_MSB << 8)
    z = Z_LSB | (Y_MSB << 8)
    
    if ( x & 0x8000 ): x = neg(x)
                
    if ( y & 0x8000 ): y = neg(y)
 
    if ( y & 0x8000 ): z = neg(z)
    
    print 'x:%d, y:%d, z:%d' % (x, y, z)
    
 #   if (y>0): hdg = 90 - atan(x/y)*180/pi
 #   if (y<0): hdg = 270 - atan(x/y)*180/pi
 #   if (y == 0 and x < 0): hdg = 180.0
 #   if (y == 0 and x > 0): hdg = 0.0

def temp_read():
    global g
    T_MSB = g.read_byte_data(gyro_addr,27)
    T_LSB = g.read_byte_data(gyro_addr,28)
    temp = T_LSB | (T_MSB << 8)
    
    if ( temp & 0x8000 ): temp = neg(temp)
                
    temp = 35 - (-13200 - temp)/280
    return temp

if __name__ == '__main__':

    gyro__init()
    while True:
        try:
            sleep(1)
            gyro_read()
            print 'temp:', temp_read()
        except KeyboardInterrupt:
            print 'Done'
            break
